#mapserver-tools

Tools for working with mapserver and OGC services

## Installation

Instaling mapscript inside a virtualenv is problematic. Therefore, for a 
quick install, install mapserver and python-mapserver using apt and then
symlink the files:

    /usr/lib/python2.7/dist-packages/_mapscript.so
    /usr/lib/python2.7/dist-packages/mapscript.py

Into the site-packages directory of your virtualenv

Eventually a more proper solution will be available
