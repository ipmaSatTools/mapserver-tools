"""Helper functions to use mapserver's request processing tools."""

import datetime as dt
import logging

import dateutil.parser
import mapscript

logger = logging.getLogger(__name__)


def process_get_request(map_obj, service, operation, **request_parameters):
    """Process an OWS request that came in via HTTP GET.

    Offloads the processing of the OWS request to MapServer.

    Parameters
    ----------

    map_obj: mapscript.mapObj
        The map object to use for processing the request
    service: str
        The OGC web service to use. Must be one of mapserver's supported
        services
    operation: str
        The name of the operation to use
    request_parameters: dict
        Additional parameters specific to each operation

    Returns
    -------

    bytes
        Mapserver buffer with the output from processing the request

    """

    ows_request = mapscript.OWSRequest()
    ows_request.setParameter("service", service)
    ows_request.setParameter("request", operation)
    for parameter_name, parameter_value in request_parameters.items():
        # should we urlencode the name and value?
        # should we cast the value to a string?
        ows_request.setParameter(parameter_name, str(parameter_value))
    mapscript.msIO_installStdoutToBuffer()
    map_obj.OWSDispatch(ows_request)
    content_type = mapscript.msIO_stripStdoutBufferContentType()
    content = mapscript.msIO_getStdoutBufferBytes()
    mapscript.msIO_resetHandlers()
    return content, content_type


def process_post_request():
    raise NotImplementedError


def get_content_type(mapserver_output_content_type):
    if mapserver_output_content_type == 'application/vnd.ogc.se_xml':
        result = 'text/xml'
    else:
        result = mapserver_output_content_type
    return result


def _parse_time_parameter(raw_time):
    """Parse the WMS Time parameter.

    According to section C.3.2 of Annex C of the spec, a temporal value can 
    take any of the following forms:

    * a single value

    * a list of multiple values, comma separated and without any white space

    * an interval in the form start/end without specifying any resolution. The
      end value is also included in the interval

    * The special value ``current``, which specifies that the server should
      return the most current data available

    Check the WMS 1.3.0 (OGC 06-042) Annexes C and D for more details

    Parameters
    ----------
    raw_time: str
        The value supplied by the client

    Returns
    -------
    list
        A sequence of ``datetime.datetime`` instances with all the parsed
        temporal instants that are to be returned

    Raises
    ------
    ValueError
        If any of the parsed temporal values cannot be parsed into a datetime
        object

    """

    temporal_slots = []
    if raw_time.lower() == "current":
        temporal_slots.append(dt.datetime.utcnow())
    else:
        interval = raw_time.split("/")
        all_values = interval if any(interval) else raw_time.split(",")
        for item in all_values:
            temporal_slots.append(dateutil.parser.parse(item))
    return temporal_slots
