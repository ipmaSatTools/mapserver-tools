from __future__ import absolute_import
import logging
import os
import re
import datetime as dt

import mapscript

logger = logging.getLogger(__name__)


def create_layer_from_layerobj(layer_obj):
    """A factory function for creating MapfileLayer objects."""
    if layer_obj.getProjection() == "":  # this is a group layer
        instance = MapfileLayerGroup.from_layer_obj(layer_obj)
    else:
        instance = MapfileLayer.from_layer_obj(layer_obj)
    return instance


def retrieve_copernicus_product_identifier(abstract):
    re_obj = re.search(r"urn:cgls:global:[\w:.]+", abstract)
    try:
        copernicus_id = re_obj.group()
    except AttributeError:
        copernicus_id = None
    return copernicus_id


def retrieve_timeslot(abstract):
    re_obj = re.search(r"_(\d{12})_", abstract)
    try:
        timeslot = dt.datetime.strptime(re_obj.group(1), "%Y%m%d%H%M%S")
    except AttributeError:
        logger.warning(
            "Could not retrieve timeslot from: {0!r}".format(abstract))
        timeslot = None
    return timeslot


def get_ows_group_path(layer_group):
    """Convert a layer group into a group path used in a mapfile's metadata.

    Parameters
    ----------
    layer_group: str
        Qualified name of the layer group to convert

    Returns
    -------
    str
        The layer group path, as used in a mapfile's ows_layer_group parameter.

    """

    if layer_group is not None:
        group_path = layer_group.replace(".", "/")
        result = "/{path}".format(path=group_path)
    else:
        result = None
    return result


def get_layer_group_name(ows_group_path):
    """Convert an ows group path into a suitable layer_group.

    Parameters
    ----------
    ows_group_path: str
        The ows group path.

    Returns
    -------
    str
        Name of the layer group.

    """

    relative_path = ows_group_path.lstrip("/")
    layer_group = relative_path.replace("/", ".")
    return layer_group


def get_node_metadata(node_obj):
    """Return a mapping with the node's metadata entries.

    A node is either a mapscript.mapObj or a mapscript.layerObj

    """
    current_key = node_obj.getNextMetaDataKey(None)
    metadata = {}
    while current_key is not None:
        metadata[current_key] = node_obj.getMetaData(current_key)
        current_key = node_obj.getNextMetaDataKey(current_key)
    return metadata


class MapfileProcessor(object):

    def __init__(self, name="", shape_path="", extent=None,
                 background_color=(0, 0, 0), title="",
                 online_resource="http://localhost"):

        self.name = name
        self.title = title
        self.shape_path = shape_path
        self.crs = "init=epsg:4326"
        self.extent = extent or mapscript.rectObj(-180, -90, 180, 90)
        self.background_color = background_color
        self.online_resource = online_resource
        self.imagetype = "png"
        self.size = (800, 600)
        self.layers = dict()

    @classmethod
    def from_mapfile(cls, mapfile_path):
        """Open an already existent mapfile."""

        previous_map_obj = mapscript.mapObj(mapfile_path)
        metadata = get_node_metadata(previous_map_obj)
        background_color = (previous_map_obj.imagecolor.red,
                            previous_map_obj.imagecolor.green,
                            previous_map_obj.imagecolor.blue)
        extent = mapscript.rectObj(
            previous_map_obj.extent.minx,
            previous_map_obj.extent.miny,
            previous_map_obj.extent.maxx,
            previous_map_obj.extent.maxy
        )
        instance = cls(
            name=previous_map_obj.name,
            shape_path=previous_map_obj.shapepath,
            extent=extent,
            background_color=background_color,
            title=metadata.get("ows_title", ""),
            online_resource=metadata.get("ows_onlineresource", ""),
        )
        for index in range(previous_map_obj.numlayers):
            layer_obj = previous_map_obj.getLayer(index)
            layer = create_layer_from_layerobj(layer_obj)
            instance.layers[layer.qualified_name] = layer
        return instance

    def save_mapfile(self, output_path="out.map"):
        directory = os.path.dirname(output_path)
        if not os.path.isdir(directory):
            os.makedirs(directory)
        mapfile = self.get_mapfile()
        mapfile.save(output_path)

    def save_image(self, output_path, debug_mapserver=False):
        mapfile = self.get_mapfile(debug_mapserver=debug_mapserver)
        img = mapfile.draw()
        if not output_path.endswith(img.format.extension):
            output_path = ".".join((output_path, img.format.extension))
        img.save(output_path)
        return output_path

    def save_legend(self, output_path):
        mapfile = self.get_mapfile()
        legend = mapfile.drawLegend()
        if not output_path.endswith(legend.format.extension):
            output_path = "{}_legend.{}".format(output_path,
                                                legend.format.extension)
        legend.save(output_path)
        return output_path

    def get_mapfile(self, debug_mapserver=False):
        """Get the representation of the mapfile."""
        map_obj = mapscript.mapObj()
        if debug_mapserver:
            map_obj.setConfigOption("MS_ERRORFILE", "stdout")
            map_obj.setConfigOption("DEBUG", "2")
            map_obj.setConfigOption("CPL_DEBUG", "ON")  # GDAL debug
            map_obj.setConfigOption("PROJ_DEBUG", "ON")  # PROJ debug
        map_obj.name = self.name
        map_obj.shapepath = self.shape_path
        map_obj.setProjection(self.crs)
        map_obj.setMetaData("ows_title", self.title or self.name)
        map_obj.setMetaData("ows_onlineresource", self.online_resource)
        # the real epsg code should be 32663, however proj4 does not include
        # it yet
        map_obj.setMetaData("ows_srs", "EPSG:4326")
        map_obj.setMetaData("ows_enable_request", "*")
        map_obj.selectOutputFormat(self.imagetype)
        map_obj.extent = self.extent
        map_obj.setSize(*self.size)
        map_obj.imagecolor = mapscript.colorObj(*self.background_color)
        for qualified_name, layer in sorted(self.layers.items(),
                                            key=lambda pair: pair[0]):
            # sorting layers by name to make it more pleasant for the user
            layer_obj = layer.get_mapscript_layer()
            map_obj.insertLayer(layer_obj)
        return map_obj


class MapfileLayerBase(object):

    def __init__(self, name, title=None, layer_group=None,
                 layer_type=mapscript.MS_LAYER_RASTER):
        self.name = name
        self.title = title or name
        self.layer_group = layer_group
        self.layer_type = layer_type
        self.status = mapscript.MS_OFF

    @property
    def qualified_name(self):
        if self.layer_group is not None:
            result = ".".join((self.layer_group, self.name))
        else:
            result = self.name
        return result

    def get_mapscript_layer(self):
        ms_layer = mapscript.layerObj()
        ms_layer.name = self.qualified_name
        ms_layer.type = self.layer_type
        ms_layer.status = self.status
        ms_layer.setMetaData('ows_title', self.title)
        if self.layer_group is not None:
            ows_group = get_ows_group_path(self.layer_group)
            ms_layer.setMetaData("ows_layer_group", ows_group)
        return ms_layer


class MapfileLayerGroup(MapfileLayerBase):

    def __repr__(self):
        return ("{0.__class__.__name__}(name={0.name!r}, title={0.title!r}, "
                "layer_type={0.layer_type!r})".format(self))

    def __str__(self):
        return "{0.__class__.__name__}({0.name})".format(self)

    @classmethod
    def from_layer_obj(cls, layer_obj):
        metadata = get_node_metadata(layer_obj)
        name = layer_obj.name.rpartition(".")[-1]
        ows_group = metadata.get("ows_layer_group")
        layer_group = get_layer_group_name(ows_group) if ows_group else None
        instance = cls(name=name, layer_type=layer_obj.type,
                       title=metadata.get("ows_title"),
                       layer_group=layer_group)
        return instance


# TODO: Might be able to use NC_GLOBAL#time_coverage_start for WMS-T
class MapfileLayer(MapfileLayerBase):

    def __init__(self, name, copernicus_product_id, data_path, projection,
                 title=None, bands=None, classes=None,
                 timeslot=None, layer_group=None, status=mapscript.MS_OFF,
                 layer_type=mapscript.MS_LAYER_RASTER):
        super(MapfileLayer, self).__init__(name,
                                           title=title,
                                           layer_group=layer_group,
                                           layer_type=layer_type)
        self.copernicus_product_id = copernicus_product_id
        self.timeslot = timeslot
        self.data_path = data_path
        self.projection = projection
        self.bands = bands
        self.classes = classes or []
        self.template = "templates/blank.html"
        self.dump = True
        self.status = status

    def __repr__(self):
        return ("{0.__class__.__name__}(name={0.name!r}, "
               "copernicus_product_id={0.copernicus_product_id!r}, "
               "data_path={0.data_path!r}, projection={0.projection!r}, "
               "title={0.title!r}, bands={0.bands!r}, classes={0.classes!r},"
                "timeslot={0.timeslot!r}, layer_group={0.layer_group!r},"
                "status={0.status!r})".format(self))

    def __str__(self):
        return ("{0.__class__.__name__}({0.qualified_name}, "
                "{0.timeslot})".format(self))

    @property
    def abstract(self):
        abstract = "Copernicus product id: {0.copernicus_product_id}".format(
            self)
        if self.timeslot is not None:
            abstract += "\nTimeslot: {}".format(
                self.timeslot.strftime("%Y-%m-%dT%H:%M:%SZ"))
        return abstract

    @classmethod
    def from_layer_obj(cls, layer_obj):
        metadata = get_node_metadata(layer_obj)
        abstract = metadata.get("ows_abstract", "")
        copernicus_id = retrieve_copernicus_product_identifier(abstract)
        timeslot = retrieve_timeslot(abstract)
        processing_bands = layer_obj.getProcessingKey("bands")
        if processing_bands is not None:
            bands = [int(b.strip()) for b in processing_bands.split(",")]
        else:
            bands = None
        class_definitions = []
        for index in range(layer_obj.numclasses):
            class_obj = layer_obj.getClass(index)
            class_definitions.append(class_obj.convertToString())
        name = layer_obj.name.rpartition(".")[-1]
        ows_group = metadata.get("ows_layer_group")
        layer_group = get_layer_group_name(ows_group) if ows_group else None
        instance = cls(
            name=name,
            data_path=layer_obj.data,
            projection=layer_obj.getProjection(),
            title=metadata.get("ows_title", ""),
            copernicus_product_id=copernicus_id,
            timeslot=timeslot,
            bands=bands,
            classes=class_definitions,
            layer_group=layer_group
        )
        return instance

    def get_mapscript_layer(self):
        ms_layer = super(MapfileLayer, self).get_mapscript_layer()

        ms_layer.template = self.template
        ms_layer.dump = mapscript.MS_TRUE if self.dump else mapscript.MS_FALSE
        ms_layer.setProjection(self.projection)
        ms_layer.setMetaData("ows_include_items", "all")
        ms_layer.setMetaData("gml_include_items", "all")
        ms_layer.setMetaData("ows_srs", "EPSG:4326")
        ms_layer.setMetaData("ows_abstract", self.abstract)
        # WMS Time support
        #ms_layer.setMetaData(
        #    "ows_timeextent",
        #    ("{timeslot:%Y-%m-%dT%H:%M:%SZ}/"
        #    "{timeslot:%Y-%m-%dT%H:%M:%SZ}".format(timeslot=self.timeslot))
        #)
        #ms_layer.setMetaData("ows_timeitem", "NC_GLOBAL#time_coverage_start")
        #ms_layer.setMetaData("ows_timedefault",
        #                     self.timeslot.strftime("%Y-%m-%dT%H:%M:%SZ"))
        ms_layer.data = self.data_path
        ms_layer.type = self.layer_type
        if self.bands:
            ms_layer.setProcessingKey("BANDS", ",".join(str(i) for i in
                                      self.bands))
        ms_layer.classitem = '[pixel]'  # TODO: generalize for non raster
        for class_definition in self.classes:
            class_obj = mapscript.classObj()
            class_obj.updateFromString(class_definition)
            ms_layer.insertClass(class_obj)
        return ms_layer

