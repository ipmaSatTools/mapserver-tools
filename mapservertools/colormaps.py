"""Color classes for generating nice mapserver layers"""

dslf_classes = [
    '''
    CLASS
        NAME "Missing value"
        EXPRESSION([pixel] == 0)
        STYLE
            COLOR 255 255 255
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "< 30 W.m^-2"
        EXPRESSION([pixel] > 0 AND [pixel] <= 30)
        STYLE
            COLOR 5 48 97
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "30 - 60 W.m^-2"
        EXPRESSION([pixel] > 30 AND [pixel] <= 60)
        STYLE
            COLOR 21 79 141
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "60 - 90 W.m^-2"
        EXPRESSION([pixel] > 60 AND [pixel] <= 90)
        STYLE
            COLOR 38 109 176
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "90 - 120 W.m^-2"
        EXPRESSION([pixel] > 90 AND [pixel] <= 120)
        STYLE
            COLOR 58 136 189
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "120 - 150 W.m^-2"
        EXPRESSION([pixel] > 120 AND [pixel] <= 150)
        STYLE
            COLOR 96 164 204
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "150 - 180 W.m^-2"
        EXPRESSION([pixel] > 150 AND [pixel] <= 180)
        STYLE
            COLOR 141 194 220
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "180 - 210 W.m^-2"
        EXPRESSION([pixel] > 180 AND [pixel] <= 210)
        STYLE
            COLOR 179 213 231
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "210 - 240 W.m^-2"
        EXPRESSION([pixel] > 210 AND [pixel] <= 240)
        STYLE
            COLOR 213 231 240
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "240 - 270 W.m^-2"
        EXPRESSION([pixel] > 240 AND [pixel] <= 270)
        STYLE
            COLOR 235 241 244
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "270 - 300 W.m^-2"
        EXPRESSION([pixel] > 270 AND [pixel] <= 300)
        STYLE
            COLOR 248 238 232
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "300 - 330 W.m^-2"
        EXPRESSION([pixel] > 300 AND [pixel] <= 330)
        STYLE
            COLOR 252 222 204
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "330 - 360 W.m^-2"
        EXPRESSION([pixel] > 330 AND [pixel] <= 360)
        STYLE
            COLOR 248 193 166
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "360 - 390 W.m^-2"
        EXPRESSION([pixel] > 360 AND [pixel] <= 390)
        STYLE
            COLOR 242 160 126
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "390 - 420 W.m^-2"
        EXPRESSION([pixel] > 390 AND [pixel] <= 420)
        STYLE
            COLOR 224 120 95
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "420 - 450 W.m^-2"
        EXPRESSION([pixel] > 420 AND [pixel] <= 450)
        STYLE
            COLOR 205 79 69
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "450 - 480 W.m^-2"
        EXPRESSION([pixel] > 450 AND [pixel] <= 480)
        STYLE
            COLOR 184 36 49
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "480 - 510 W.m^-2"
        EXPRESSION([pixel] > 480 AND [pixel] <= 510)
        STYLE
            COLOR 147 14 38
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "> 510 W.m^-2"
        EXPRESSION([pixel] > 510)
        STYLE
            COLOR 103 0 31
        END  # STYLE
    END  # CLASS
    ''',
]

dssf_classes = [
    '''
    CLASS
        NAME "Missing value"
        EXPRESSION([pixel] == -1)
        STYLE
            COLOR 255 255 255
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "< 100 W.m^-2"
        EXPRESSION([pixel] > -1 AND [pixel] <= 100)
        STYLE
            COLOR 0 0 0
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "100 - 200 W.m^-2"
        EXPRESSION([pixel] > 100 AND[pixel] <= 200)
        STYLE
            COLOR 0 0 41
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "200 - 300 W.m^-2"
        EXPRESSION([pixel] > 200 AND [pixel] <= 300)
        STYLE
            COLOR 0 25 83
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "300 - 400 W.m^-2"
        EXPRESSION([pixel] > 300 AND [pixel] <= 400)
        STYLE
            COLOR 0 87 100
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "400 - 500 W.m^-2"
        EXPRESSION([pixel] > 400 AND [pixel] <= 500)
        STYLE
            COLOR 0 150 100
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "500 - 600 W.m^-2"
        EXPRESSION([pixel] > 500 AND [pixel] <= 600)
        STYLE
            COLOR 0 147 37
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "600 - 700 W.m^-2"
        EXPRESSION([pixel] > 600 AND [pixel] <= 700)
        STYLE
            COLOR 60 120 0
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "700 - 800 W.m^-2"
        EXPRESSION([pixel] > 700 AND [pixel] <= 800)
        STYLE
            COLOR 180 25 0
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "800 - 900 W.m^-2"
        EXPRESSION([pixel] > 800 AND [pixel] <= 900)
        STYLE
            COLOR 208 36 0
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "900 - 1000 W.m^-2"
        EXPRESSION([pixel] > 900 AND [pixel] <= 1000)
        STYLE
            COLOR 218 83 0
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "1000 - 1100 W.m^-2"
        EXPRESSION([pixel] > 1000 AND [pixel] <= 1100)
        STYLE
            COLOR 228 129 0
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "1100 - 1200 W.m^-2"
        EXPRESSION([pixel] > 1100 AND [pixel] <= 1200)
        STYLE
            COLOR 238 175 0
        END  # STYLE
    END  # CLASS
    ''',
    '''
    CLASS
        NAME "> 1200 W.m^-2"
        EXPRESSION([pixel] > 1200)
        STYLE
            COLOR 248 221 0 
        END  # STYLE 
    END  # CLASS
    ''',
]

lst_classes = [
    '''
    CLASS
      NAME "value == -8000"
      EXPRESSION ([pixel] == -8000)
      STYLE
          COLOR 255 255 255
      END
    END
    ''',
    '''
    CLASS
      NAME "-100 <= value < -40"
      EXPRESSION ([pixel] > -10273.15 AND [pixel] <= -4273.15)
      STYLE
        COLOR 76 76 76
      END
    END
    ''',
    '''
    CLASS
      NAME "-40 <= value < -35"
      EXPRESSION ([pixel] > -4273.15 AND [pixel] <= -3773.15)
      STYLE
        COLOR 161 161 161
      END
    END
    ''',
    '''
    CLASS
      NAME "-35 <= value < -30"
      EXPRESSION ([pixel] > -3773.15 AND [pixel] <= -3273.15)
      STYLE
        COLOR 245 245 245
      END
    END
    ''',
    '''
    CLASS
      NAME "-30 <= value < -25"
      EXPRESSION ([pixel] > -3273.15 AND [pixel] <= -2773.15)
      STYLE
        COLOR 190 147 212
      END
    END
    ''',
    '''
    CLASS
      NAME "-25 <= value < -20"
      EXPRESSION ([pixel] > -2773.15 AND [pixel] <= -2273.15)
      STYLE
        COLOR 118 26 163
      END
    END
    ''',
    '''
    CLASS
      NAME "-20 <= value < -15"
      EXPRESSION ([pixel] > -2273.15 AND [pixel] <= -1773.15)
      STYLE
        COLOR 68 29 141
      END
    END
    ''',
    '''
    CLASS
      NAME "-15 <= value < -10"
      EXPRESSION ([pixel] > -1773.15 AND [pixel] <= -1273.15)
      STYLE
        COLOR 24 66 127
      END
    END
    ''',
    '''
    CLASS
      NAME "-10 <= value < -5"
      EXPRESSION ([pixel] > -1273.15 AND [pixel] <= -773.15)
      STYLE
        COLOR 15 93 86
      END
    END
    ''',
    '''
    CLASS
      NAME "-5 <= value < 0"
      EXPRESSION ([pixel] > -773.15 AND [pixel] <= -273.15)
      STYLE
        COLOR 24 115 31
      END
    END
    ''',
    '''
    CLASS
      NAME "0 <= value < 5"
      EXPRESSION ([pixel] > -273.15 AND [pixel] <= 226.85)
      STYLE
        COLOR 83 155 10
      END
    END
    ''',
    '''
    CLASS
      NAME "5 <= value < 10"
      EXPRESSION ([pixel] > 226.85 AND [pixel] <= 726.85)
      STYLE
        COLOR 182 109 15
      END
    END
    ''',
    '''
    CLASS
      NAME "10 <= value < 15"
      EXPRESSION ([pixel] > 726.85 AND [pixel] <= 1226.85)
      STYLE
        COLOR 240 210 14
      END
    END
    ''',
    '''
    CLASS
      NAME "15 <= value < 20"
      EXPRESSION ([pixel] > 1226.85 AND [pixel] <= 1726.85)
      STYLE
        COLOR 249 145 5
      END
    END
    ''',
    '''
    CLASS
      NAME "20 <= value < 25"
      EXPRESSION ([pixel] > 1726.85 AND [pixel] <= 2226.85)
      STYLE
        COLOR 255 85 0
      END
    END
    ''',
    '''
    CLASS
      NAME "25 <= value < 30"
      EXPRESSION ([pixel] > 2226.85 AND [pixel] <= 2726.85)
      STYLE
        COLOR 255 37 0
      END
    END
    ''',
    '''
    CLASS
      NAME "30 <= value < 35"
      EXPRESSION ([pixel] > 2726.85 AND [pixel] <= 3226.85)
      STYLE
        COLOR 238 37 0
      END
    END
    ''',
    '''
    CLASS
      NAME "35 <= value < 40"
      EXPRESSION ([pixel] > 3226.85 AND [pixel] <= 3726.85)
      STYLE
        COLOR 238 3 1
      END
    END
    ''',
    '''
    CLASS
      NAME "40 <= value < 45"
      EXPRESSION ([pixel] > 3726.85 AND [pixel] <= 4226.85)
      STYLE
        COLOR 164 16 8
      END
    END
    ''',
    '''
    CLASS
      NAME "45 <= value < 50"
      EXPRESSION ([pixel] > 4226.85 AND [pixel] <= 4726.85)
      STYLE
        COLOR 96 27 15
      END
    END
    ''',
    '''
    CLASS
      NAME "50 <= value < 55"
      EXPRESSION ([pixel] > 4726.85 AND [pixel] <= 5226.85)
      STYLE
        COLOR 73 25 19
      END
    END
    ''',
    '''
    CLASS
      NAME "value > 55"
      EXPRESSION ([pixel] > 5226.85)
      STYLE
        COLOR 51 22 22
      END
    END
    ''',
]

percent_proc_pixels_classes = [
    '''
    CLASS
      NAME "value == -1"
      EXPRESSION ([pixel] == -1)
      STYLE
          COLOR 255 255 255
      END
    END
    ''',
    '''
    CLASS
      NAME "0 <= value < 20"
      EXPRESSION ([pixel] >= 0 AND [pixel] < 20)
      STYLE
        COLOR 237 248 251
      END
    END
    ''',
    '''
    CLASS
      NAME "20 <= value < 40"
      EXPRESSION ([pixel] >= 20 AND [pixel] < 40)
      STYLE
        COLOR 178 226 226
      END
    END
    ''',
    '''
    CLASS
      NAME "40 <= value < 60"
      EXPRESSION ([pixel] >= 40 AND [pixel] < 60)
      STYLE
        COLOR 102 194 164
      END
    END
    ''',
    '''
    CLASS
      NAME "60 <= value < 80"
      EXPRESSION ([pixel] >= 60 AND [pixel] < 80)
      STYLE
        COLOR 44 162 95
      END
    END
    ''',
    '''
    CLASS
      NAME "80 <= value <= 100"
      EXPRESSION ([pixel] >= 80 AND [pixel] <= 100)
      STYLE
        COLOR 0 109 44
      END
    END
    ''',
]

q_flags_classes = [
    '''
    CLASS
      NAME "out of disk"
      EXPRESSION ([pixel] == 32)
      STYLE
          COLOR 255 255 255
      END
    END
    ''',
    '''
    CLASS
      NAME "GOES sea pixel"
      EXPRESSION ([pixel] == 0)
      STYLE
          COLOR 189 215 231
      END
    END
    ''',
    '''
    CLASS
      NAME "MSG sea pixel"
      EXPRESSION ([pixel] == 1)
      STYLE
          COLOR 107 174 214
      END
    END
    ''',
    '''
    CLASS
      NAME "HIMAWARI sea pixel"
      EXPRESSION ([pixel] == 2)
      STYLE
          COLOR 49 130 189
      END
    END
    ''',
    '''
    CLASS
      NAME "multi-mission sea pixel"
      EXPRESSION ([pixel] == 3)
      STYLE
          COLOR 8 81 156
      END
    END
    ''',
    '''
    CLASS
      NAME "GOES cloud free pixel"
      EXPRESSION ([pixel] == 4)
      STYLE
          COLOR 203 201 226
      END
    END
    ''',
    '''
    CLASS
      NAME "MSG cloud free pixel"
      EXPRESSION ([pixel] == 5)
      STYLE
          COLOR 158 154 200
      END
    END
    ''',
    '''
    CLASS
      NAME "HIMAWARI cloud free pixel"
      EXPRESSION ([pixel] == 6)
      STYLE
          COLOR 117 107 177
      END
    END
    ''',
    '''
    CLASS
      NAME "multi-mission cloud free pixel"
      EXPRESSION ([pixel] == 7)
      STYLE
          COLOR 84 39 143
      END
    END
    ''',
    '''
    CLASS
      NAME "GOES cloud contaminated pixel"
      EXPRESSION ([pixel] == 8)
      STYLE
          COLOR 215 181 216
      END
    END
    ''',
    '''
    CLASS
      NAME "MSG cloud contaminated pixel"
      EXPRESSION ([pixel] == 9)
      STYLE
          COLOR 223 101 176
      END
    END
    ''',
    '''
    CLASS
      NAME "HIMAWARI cloud contaminated pixel"
      EXPRESSION ([pixel] == 10)
      STYLE
          COLOR 221 28 119
      END
    END
    ''',
    '''
    CLASS
      NAME "multi-mission cloud contaminated pixel"
      EXPRESSION ([pixel] == 11)
      STYLE
          COLOR 152 0 67
      END
    END
    ''',
    '''
    CLASS
      NAME "GOES cloud filled pixel"
      EXPRESSION ([pixel] == 12)
      STYLE
          COLOR 204 204 204
      END
    END
    ''',
    '''
    CLASS
      NAME "MSG cloud filled pixel"
      EXPRESSION ([pixel] == 13)
      STYLE
          COLOR 150 150 150
      END
    END
    ''',
    '''
    CLASS
      NAME "HIMAWARI cloud filled pixel"
      EXPRESSION ([pixel] == 14)
      STYLE
          COLOR 99 99 99
      END
    END
    ''',
    '''
    CLASS
      NAME "multi-mission cloud filled pixel"
      EXPRESSION ([pixel] == 15)
      STYLE
          COLOR 37 37 37
      END
    END
    ''',
    '''
    CLASS
      NAME "GOES unprocessed pixel"
      EXPRESSION ([pixel] == 16)
      STYLE
          COLOR 186 228 179
      END
    END
    ''',
    '''
    CLASS
      NAME "MSG unprocessed pixel"
      EXPRESSION ([pixel] == 17)
      STYLE
          COLOR 116 196 118
      END
    END
    ''',
    '''
    CLASS
      NAME "HIMAWARI unprocessed pixel"
      EXPRESSION ([pixel] == 18)
      STYLE
          COLOR 49 163 84
      END
    END
    ''',
    '''
    CLASS
      NAME "multi-mission unprocessed pixel"
      EXPRESSION ([pixel] == 19)
      STYLE
          COLOR 0 109 44
      END
    END
    ''',
]

errorbar_classes = [
    '''
    CLASS
      NAME "missing value"
      EXPRESSION ([pixel] == -1)
      STYLE
          COLOR 255 255 255
      END
    END
    ''',
    '''
    CLASS
      NAME "0 <= value < 5"
      EXPRESSION ([pixel] >= 0 AND [pixel] < 500)
      STYLE
        COLOR 255 245 240
      END
    END
    ''',
    '''
    CLASS
      NAME "5 <= value < 10"
      EXPRESSION ([pixel] >= 500 AND [pixel] < 1000)
      STYLE
        COLOR 254 224 210
      END
    END
    ''',
    '''
    CLASS
      NAME "10 <= value < 15"
      EXPRESSION ([pixel] >= 1000 AND [pixel] < 1500)
      STYLE
        COLOR 252 187 161
      END
    END
    ''',
    '''
    CLASS
      NAME "15 <= value < 20"
      EXPRESSION ([pixel] >= 1500 AND [pixel] < 2000)
      STYLE
        COLOR 252 146 114
      END
    END
    ''',
    '''
    CLASS
      NAME "20 <= value < 25"
      EXPRESSION ([pixel] >= 2000 AND [pixel] < 2500)
      STYLE
        COLOR 251 106 74
      END
    END
    ''',
    '''
    CLASS
      NAME "25 <= value < 30"
      EXPRESSION ([pixel] >= 2500 AND [pixel] < 3000)
      STYLE
        COLOR 239 59 44
      END
    END
    ''',
    '''
    CLASS
      NAME "30 <= value < 35"
      EXPRESSION ([pixel] >= 3000 AND [pixel] < 3500)
      STYLE
        COLOR 203 24 29
      END
    END
    ''',
    '''
    CLASS
      NAME "35 <= value < 40"
      EXPRESSION ([pixel] >= 3500 AND [pixel] < 4000)
      STYLE
        COLOR 165 15 21
      END
    END
    ''',
    '''
    CLASS
      NAME "40 <= value < 45"
      EXPRESSION ([pixel] >= 4000 AND [pixel] < 4500)
      STYLE
        COLOR 103 0 13
      END
    END
    ''',
    '''
    CLASS
      NAME "45 <= value < 50"
      EXPRESSION ([pixel] >= 4500 AND [pixel] < 5000)
      STYLE
        COLOR 82 0 10
      END
    END
    ''',
    '''
    CLASS
      NAME "value >= 50"
      EXPRESSION ([pixel] => 5000)
      STYLE
        COLOR 0 0 0
      END
    END
    ''',
]


time_delta_classes  = [
    '''
    CLASS
      NAME "value == -99"
      EXPRESSION ([pixel] == -99)
      STYLE
          COLOR 255 255 255
      END
    END
    ''',
    '''
    CLASS
      NAME "-30 <= value < -15"
      EXPRESSION ([pixel] >= -30 AND [pixel] < -15)
      STYLE
        COLOR 237 248 251
      END
    END
    ''',
    '''
    CLASS
      NAME "-15 <= value < 0"
      EXPRESSION ([pixel] >= -15 AND [pixel] < 0)
      STYLE
        COLOR 179 205 227
      END
    END
    ''',
    '''
    CLASS
      NAME "0 <= value < 15"
      EXPRESSION ([pixel] >= 0 AND [pixel] < 15)
      STYLE
        COLOR 140 150 198
      END
    END
    ''',
    '''
    CLASS
      NAME "15 <= value < 30"
      EXPRESSION ([pixel] >= 15 AND [pixel] <= 30)
      STYLE
        COLOR 136 65 157
      END
    END
    ''',
]


frac_valid_obs_classes = [
    '''
    CLASS
      NAME "missing value"
      EXPRESSION ([pixel] == -8000)
      STYLE
          COLOR 255 255 255
      END
    END
    ''',
    '''
    CLASS
      NAME "0 <= value < 0.2"
      EXPRESSION ([pixel] >= 0 AND [pixel] < 0.2)
      STYLE
        COLOR 237 248 251
      END
    END
    ''',
    '''
    CLASS
      NAME "0.2 <= value < 0.4"
      EXPRESSION ([pixel] >= 0.2 AND [pixel] < 0.4)
      STYLE
        COLOR 178 226 226
      END
    END
    ''',
    '''
    CLASS
      NAME "0.4 <= value < 0.6"
      EXPRESSION ([pixel] >= 0.4 AND [pixel] < 0.6)
      STYLE
        COLOR 102 194 164
      END
    END
    ''',
    '''
    CLASS
      NAME "0.6 <= value < 0.8"
      EXPRESSION ([pixel] >= 0.6 AND [pixel] < 0.8)
      STYLE
        COLOR 44 162 95
      END
    END
    ''',
    '''
    CLASS
      NAME "0.8 <= value <= 1"
      EXPRESSION ([pixel] >= 0.8 AND [pixel] <= 1)
      STYLE
        COLOR 0 109 44
      END
    END
    ''',
]

colormap = {
    "dslf_dslf": dslf_classes,
    "dslf_q_flags": q_flags_classes,
    "dslf_percent_proc_pixels": percent_proc_pixels_classes,
    "dslf_time_delta": time_delta_classes,
    "dssf_dssf": dssf_classes,
    "dssf_q_flags": q_flags_classes,
    "dssf_percent_proc_pixels": percent_proc_pixels_classes,
    "dssf_time_delta": time_delta_classes,
    "lst_lst": lst_classes,
    "lst_q_flags": q_flags_classes,
    "lst_percent_proc_pixels": percent_proc_pixels_classes,
    "lst_errorbar_lst": errorbar_classes,
    "lst_time_delta": time_delta_classes,
    "lst10-dc_max": lst_classes,
    "lst10-dc_min": lst_classes,
    "lst10-dc_median": lst_classes,
    "lst10_frac_valid_obs": frac_valid_obs_classes,
    "lst10-tci_max": lst_classes,
    "lst10-tci_min": lst_classes,
    "lst10-tci_median": lst_classes,
    "lst10-tci_frac_valid_obs": frac_valid_obs_classes,
}

