import io
from os.path import dirname, join

from setuptools import find_packages
from setuptools import setup


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get("encoding", "utf-8")
    ).read()

setup(
    name='mapservertools',
    version=read("VERSION"),
    description='',
    long_description='',
    author='Ricardo Silva',
    author_email='ricardo.silva@ipma.pt',
    url='',
    classifiers=[''],
    platforms=[''],
    license='',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        #'mapscript',
        "python-dateutil",
    ]
)
