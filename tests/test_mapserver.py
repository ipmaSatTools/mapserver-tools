"""Unit tests for mapservertools.mapserver."""

import datetime as dt

import pytest

from mapservertools import mapserver

@pytest.mark.parametrize("layer_group,expected", [
    ("group1", "/group1"),
    ("group1.group2", "/group1/group2"),
    ("group1.group2.group3", "/group1/group2/group3"),
])
def test_get_ows_group_path(layer_group, expected):
    result = mapserver.get_ows_group_path(layer_group)
    assert result == expected


@pytest.mark.parametrize("ows_group_path,expected", [
    ("/group1", "group1"),
    ("/group1/group2", "group1.group2"),
    ("/group1/group2/group3", "group1.group2.group3"),
])
def test_get_layer_group_name(ows_group_path, expected):
    layer_group = mapserver.get_layer_group_name(ows_group_path)
    assert layer_group == expected


@pytest.mark.parametrize("abstract,expected", [
    ("fake abstract with _201601010000_ and more", dt.datetime(2016, 1, 1))
])
def test_retrieve_timeslot(abstract, expected):
    result = mapserver.retrieve_timeslot(abstract)
    assert result == expected


@pytest.mark.parametrize("abstract,expected", [
    ("fake abstract with urn:cgls:global:lst_v1_0.045degree:LST_201611021200_"
     "GLOBE_GEO_V1.2 and more",
     "urn:cgls:global:lst_v1_0.045degree:LST_201611021200_GLOBE_GEO_V1.2")
])
def test_retrieve_copernicus_product_identifier(abstract, expected):
    result = mapserver.retrieve_copernicus_product_identifier(abstract)
    assert result == expected


class TestMapfileLayer(object):

    def test_creation_mandatory_parameters(self):
        fake_name = "fake"
        fake_id = "fake_id"
        fake_data_path = "fake_data_path"
        fake_projection = "fake_projection"
        layer = mapserver.MapfileLayer(
            name=fake_name,
            copernicus_product_id=fake_id,
            data_path=fake_data_path,
            projection=fake_projection,
        )
        assert layer.name == fake_name
        assert layer.copernicus_product_id == fake_id
        assert layer.data_path == fake_data_path
        assert layer.projection == fake_projection

    @pytest.mark.parametrize("copernicus_id,timeslot,expected", [
        ("fake_id", None, "Copernicus product id: fake_id"),
        ("fake_id", dt.datetime(2016, 1, 1), "Copernicus product id: fake_id\n"
                                             "Timeslot: 2016-01-01T00:00:00Z"),
    ])
    def test_layer_abstract(self, copernicus_id, timeslot, expected):
        layer = mapserver.MapfileLayer(
            name="fake_name",
            copernicus_product_id=copernicus_id,
            data_path="fake_path",
            projection="fake_projection",
            timeslot=timeslot,
        )
        assert layer.abstract == expected

    @pytest.mark.parametrize("layer_group,layer_name,expected", [
        ("group1", "name", "group1.name"),
        ("group1.group2", "name", "group1.group2.name"),
        (None, "name", "name"),
    ])
    def test_layer_qualified_name(self, layer_group, layer_name, expected):
        layer = mapserver.MapfileLayer(
            name=layer_name,
            copernicus_product_id="fake_id",
            data_path="fake_path",
            projection="fake_projection",
            layer_group=layer_group,
        )
        assert layer.qualified_name == expected

    @pytest.mark.parametrize("name,title,expected", [
        ("name", "title", "title"),
        ("name", None, "name"),
    ])
    def test_layer_title(self, name, title, expected):
        layer = mapserver.MapfileLayer(
            name=name,
            copernicus_product_id="fake_id",
            data_path="fake_path",
            projection="fake_projection",
            title=title
        )
        assert layer.title == expected

