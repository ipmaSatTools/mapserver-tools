"""Unit tests for mapservertools.requestprocessor."""

import datetime as dt

import mock
import pytest

from mapservertools import requestprocessor


@pytest.mark.parametrize("raw_time, expected", [
    ("2017-05-13", [dt.datetime(2017, 5, 13)]),
    ("2017-05-13T12:00:00", [dt.datetime(2017, 5, 13, 12)]),
])
def test_parse_time_parameter(raw_time, expected):
    result = requestprocessor._parse_time_parameter(raw_time)
    assert result == expected


def test_parse_time_parameter_current():
    fake_now = dt.datetime(2017, 1, 1)
    with mock.patch("mapservertools.requestprocessor.dt",
                    autospec=True) as mock_dt:
        mock_now = mock_dt.datetime.utcnow
        mock_now.return_value = fake_now
        result = requestprocessor._parse_time_parameter("current")
        assert result == [fake_now]
